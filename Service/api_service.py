from selenium import webdriver
import requests
from requests.auth import HTTPBasicAuth
import logging
import json
from vertify import Vert

def post_req(url,payload,auth=None,cookie=None, header=None):
    logging.info("POST: "+ url)
    logging.info("Payload: " + str(payload))
    response=""
    if(header == None):
        logging.error('header is empty')
    else:
        try:
            response = requests.post(url=url,headers=header,data=payload)
        except Exception as e: 
            logging.error("Post Fail" + str(e))
            return { "error": e}

        return response
    

def get_req(url,payload=None,auth=None,cookie=None, header=None):
    logging.info("GET: "+ url)
    logging.info("Payload: " + str(payload))
    response=""
    try:
        response = requests.get(url=url,headers=header,data=payload)
    except Exception as e: 
        logging.error("Get Fail" + str(e))
        return { "error": e}
    return response
    