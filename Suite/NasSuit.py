from selenium import webdriver
from Page.Nas_page import NasPage
from Page.base_page import BasePage
from Model.NasFlow_obj import NasOptionTree
from Service.chrome_service import devService
from url_data import GlobSet
from vertify import *
import unittest,time
import pychrome


class NasTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(chrome_options= GlobSet.op)
        dev_tool = pychrome.Browser(url="http://127.0.0.1:9222")
        cls.dev_tab = dev_tool.list_tab()[0]
        cls.dev_tab.start()
        
        cls.nasPage = NasPage(cls.driver)
        cls.vertify = Vert(cls)


    def setUp(self):
        self.driver.get(GlobSet('nas').domain)
        logging.info(self._testMethodName)
        self.vertify.result = True
        self.vertify.case_key = self._testMethodName
        

    def go_through_flattree(self,nastree, multiple = False):
        app_visited=[]
        print(nastree)
        for index,node in enumerate(nastree):

            if node[1]=='Root':
                continue
            
            #Verify every element option text
            func =  getattr(self.nasPage,"get_" + node[2])
            vert_option_text = func(node[1])
            self.vertify.vert("Equal",node[0],vert_option_text)
            print(vert_option_text)

            if "user_" in node[1]:
               self.nasPage.click_label_option(node[1])
            #Click next after clicking the app type suboption(資料被分,生產力工具...)
            elif "app_" in node[1]:
                app_visited.append(node)
                self.nasPage.click_label_option(node[1])
                time.sleep(2)
                self.nasPage.click_next()
                time.sleep(1)

            #Boundary Check ,  click back page after vertified answer and next option is main/type option
            if index!=len(nastree)-1 and node[2]== "answer" and "option" in nastree[index+1][2] :
                self.nasPage.click_back()
                #Unselect the option if the case is not multiple
                if multiple == False:    
                    self.nasPage.click_label_option(app_visited.pop()[1])  

        return app_visited
    
    def test_facebook_pixel_network(self):
        devservice = devService()
        self.dev_tab.Network.enable()
        self.dev_tab.Page.navigate(url=GlobSet('nas').domain,_timeout=5)
        devservice.filter_domain = "www.facebook.com"
        self.dev_tab.Network.requestWillBeSent = devservice.request_filter
        time.sleep(10)
        self.vertify.vert('Equal',6, len(devservice.request))
        self.dev_tab.Network.disable()
    
    
    def test_nas_flow_content(self):
        nas = NasOptionTree()
        self.go_through_flattree(nas.GetFlatenTree())
    

    def test_nas_flow_multiple_choice_content(self):
         nassub = NasOptionTree()
         nassub.SetSubData(limit=4,label = 'user_type_business')

         flat_subtree = nassub.GetFlatenTree(data = nassub.sub_data)
         flat_sub_parent = flat_subtree.copy()
         flat_sub_parent = nassub.SetParent(data = flat_sub_parent)
         flat_subtree.sort(key=lambda x:x[1])
         logging.info("Template Data {}".format(flat_subtree))
         app_visited = self.go_through_flattree(flat_sub_parent,multiple=True)
         

         page_ques_flat = self.nasPage.get_multi_ques() + app_visited
         page_ques_flat.sort(key=lambda x:x[1])
         logging.info("Page Data {}".format(page_ques_flat))
         
         self.vertify.vert('Equal',str(page_ques_flat),str(flat_subtree))

    @classmethod
    def tearDownClass(cls):
         cls.driver.close()   
         #cls.dev_tab.stop()