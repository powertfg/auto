import unittest ,logging,sys,json
from vertify import Vert
import url_data as url
from Model.Swapi_obj import Swapi


class SwapiTest(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.vertify = Vert(cls)

    def setUp(self):
        logging.info(self._testMethodName)
        self.vertify.result = True
        self.vertify.case_key = self._testMethodName
    
    def test_distinct_species_001(self):
       response =  Swapi.get_film("6")
       if(self.vertify.vertApI(response=response)):
            self.vertify.vert("NotEqual",len(response.json()["species"]),0,"response is not empty")
            logging.info("Numbers of species: "+ str(len(response.json()["species"])))
           
        
    def test_all_film_sort_episode_002(self):
        response = Swapi.get_all_films()
        if(self.vertify.vertApI(response=response)):
            self.vertify.vert("NotEqual",len(response.json()["results"]), 0, "response is not empty")
            sorted_list = sorted(json.loads(response.text)['results'],key=lambda f: f['episode_id'],reverse=False)
            logging.debug(sorted_list)
            logging.info("Sorted Data")
            logging.info("Pass API:"+json.dumps(list(map(lambda x: { "title":x['title'], "episode_id":x['episode_id']},sorted_list))))

    def test_vehicles_max_atmosphering_speed_003(self):
        response = Swapi.get_vehicles()
        if(self.vertify.vertApI(response=response)):
            self.vertify.vert("NotEqual",len(response.json()["results"]), 0,"response is not empty")
            sorted_list = [ vehicle for vehicle in response.json()['results'] if int(vehicle['max_atmosphering_speed'])>1000 ]
            logging.info("Sorted Data")
            logging.info("Pass API:"+ json.dumps(sorted_list))

    def tearDown(self):
        #Check if this test has pass with multipule assertions
        self.assertTrue(self.vertify.result)
        self.vertify.case_assert.clear()