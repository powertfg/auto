import url_data as url
from  Page.Auth_page import AuthPage
from Page.base_page import BasePage
from Model.AuthUser_obj import AuthUser
import unittest
from selenium import webdriver
import logging, sys, time ,re
from vertify import *
from url_data import GlobSet


class AuthTest(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        cls.user = AuthUser()
        cls.driver = webdriver.Chrome(chrome_options= GlobSet.op)
        cls.driver.get(GlobSet.domain+"auth/login/")
        cls.AuthPage = AuthPage(cls.driver)
        cls.vertify = Vert(cls)

    def setUp(self):
        logging.info(self._testMethodName)
        self.vertify.result = True
        self.vertify.case_key = self._testMethodName

    def test_Basic_Login_3325(self):
        self.user.get_token()
        self.AuthPage.Web_Basic_Login(self.user)
        self.vertify.vert("Equal",self.user.username, self.AuthPage.get_account_name())
        self.maxDiff = None
        self.vertify.vert("NotEqual", None, self.AuthPage.get_localstorage('token'))
    
    def test_Logout_3326(self):
        logintoken = self.AuthPage.get_localstorage('token')
        self.AuthPage.go_to_account()
        self.AuthPage.click_logout()
        time.sleep(2)
        token = self.AuthPage.get_localstorage('token')
        self.vertify.vert("NotEqual",logintoken , token)

    def tearDown(self):
        #Check if this test has pass with multipule assertions
        self.assertTrue(self.vertify.result)
        self.vertify.case_assert.clear()

    @classmethod
    def tearDownClass(cls):
         cls.driver.close()   