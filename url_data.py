import json,sys
from selenium.webdriver.chrome.options import Options
def get_domain(env):

    domain = {
        "qa": "http://acnet-qa.atgames.net/",
        "live":"https://www.atgames.net/arcadenet/",
        "swapi":"http://swapi.dev/api/",
        "nas": "https://www.synology.com/zh-tw/support/nas_selector",
    }
    return domain[env]

def path(pathname):
    path = {
        "leaderboard": "d2d/archade/v1/leaderboards/uploads",
        "signin": "backend/api/account/sign_in",
        "films":"films/",
        "vehicles":"vehicles/"
    }
    return path[pathname]

class GlobSet():

    domain= get_domain("live")
    op = Options()

    def __init__(self,env):
        self.domain = get_domain(env)
    
    def enable_headless(self):
        self.op.add_argument('--no-sandbox')
        self.op.add_argument('--disable-dev-shm-usage')
        self.op.add_argument('--remote-debugging-port=9222')
        self.op.add_argument('--allow-running-insecure-content')
        self.op.add_argument("--start-maximized")
        self.op.add_argument("--disable-gpu")
        self.op.add_argument("--disable-extensions")
        self.op.add_experimental_option("useAutomationExtension", False)
        self.op.add_argument("--proxy-server='direct://'")
        self.op.add_argument("--proxy-bypass-list=*")
        self.op.add_argument("--start-maximized")
        self.op.add_argument("--headless")

header={
        "Host":"www.atgames.net",
        "Cookie":"_d2d_session=73b80057b0cd3f93a254112013ed2043",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection":"keep-alive",
        "devise" : "Arcade",
        "fp": "NDBkNDQxYmY4NTVjMmRiODMwZWFlNGVlOGNhNzRkOWY=",
        "Content-Type":"application/json",
        "Accept":"application/json, text/plain, */*",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36"
}