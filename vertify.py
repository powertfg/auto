from unittest import TestCase, TextTestResult
import logging
import sqlite3,json
from url_data import GlobSet
class Vert():

    result = True
    case_assert = []

    def __init__(self,test):
        self.test = test()

    def vert(self,type,value1,value2=None,msg=None):
        try:
            if(type=="Equal"):
                self.test.assertEqual(first=value1,second=value2, msg=None)
                logging.info("Pass: {} is Equal to {} ".format(value1,value2))
                
            if(type=="NotEqual"):
                self.test.assertNotEqual(first=value1, second=value2,msg=None)
                logging.info("Pass: {} is Not Equal to {} {}".format(value1,value2,msg))
               
            if(type=="NotNone"):
                self.test.assertIsNotNone(value1,msg=None)
                logging.info("Pass: {} is NotNone ".format(value1))
              
            if(type=="In"):
                self.test.assertIn(value1,value2)
            
            if(type=='ListEqual'):
                self.test.assertListEqual(value1,value2,msg=None)
                logging.info("Pass: {} List is Equal to {} ".format(value1,value2))
            
            return None

        except AssertionError as e:
            logging.error("Fail: "+ str(e))
            self.result = False
            self.case_assert.append(str(type) + str(e))
            return AssertionError

    def vertApI(self,response,compare=None):

        try:
            self.test.assertIsNotNone(response)
            logging.info("Pass: response is NotNone ")
        except AssertionError as e:
            logging.error("Fail:" +str(e))
            return False

        try:
            self.test.assertLessEqual(response.status_code,201)
            logging.info("Pass API:"+ response.text.replace("'"," "))
            try: 
                response.json()
                return True
            except Exception as e:
                logging.error("Fail: Unable to transfer to json "+str(e))
                return False
            
        except AssertionError as e:
            logging.error("Fail: Status Code " +str(e) + response.text.replace("'"," "))
            return False
           
            