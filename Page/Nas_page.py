from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from Page.base_page import BasePage 
import time
class NasPage(BasePage):

    def click_label_option(slef,label):
        print(label)
        super().get_ele(By.XPATH,'//label[@for="'+label +'"]',"Click on option:"+label).click()

    def get_main_option(self,label):
        return super().get_ele(By.XPATH,'//label[@for="'+label +'"]/div',"Get Main Option").text

    def get_type_option(self,label):
        return super().get_ele(By.XPATH,'//label[@for="'+label +'"]/div/div[1]',"Get Option").text

    def get_answer(self,label):
        return super().get_ele(By.XPATH,'//label[@for="'+label +'"]/div/div',"Get Option").text

    def get_ques(self,label):
        return super().get_ele(By.XPATH,'//div[@data-question-key="'+ label+ '"]/section/div[1]/h4/div',"Get Question").text

    def click_next(self):
        super().get_ele(By.XPATH,'//*[@id="what_app_you_run"]/div[2]/button',"Click Next").click()
        
    def click_back(self):
        #ele = self.driver.find_element_by_xpath('//*[@id="checkform"]/div[2]')
        self.driver.execute_script("window.scrollTo(0,0)")
        time.sleep(2)
        super().get_ele(By.XPATH, '//*[@id="ques"]/ul/li/a',"Click Back to Nas select").click()



    def get_multi_ques(self):
        eles = super().get_multi_ele(By.CLASS_NAME,'bigQ_unit','Get Question Section')
        ele_data = []
        for ele in eles:
            label = ele.get_attribute('data-question-key')
            text  = self.get_ques(label)
            ele_data.append((text,label,'ques'))

            ans_ele = ele.find_elements(By.CLASS_NAME,'nas_s_lab')
            for ele in ans_ele:
                label = ele.get_attribute('for')
                text = self.get_answer(label)
                ele_data.append((text,label,'ans'))
                
        return ele_data
            
            



        
    