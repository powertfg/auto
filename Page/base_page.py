import time
import os
import logging

from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException, JavascriptException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
"""
Util functions
"""
#Check whether element is a displayed ele, with width>0 and height>0
def ele_displayed(ele):
    try:
        return ele.is_displayed and ele.size["width"] > 0 and ele.size["height"] > 0
    except StaleElementReferenceException:
        return False

"""
Main Funtions
""" 
class BasePage():

    def __init__(self, driver):
        self.driver = driver
    def __set__(self,driver):
         self.driver = driver


    def get_multi_ele(self,by,locator_value,action):
        try:
            WebDriverWait(self.driver, 20).until(expected_conditions.visibility_of_any_elements_located((by, locator_value)),
                                                message="[{}] Fail to get parent element {}:{}".format(action,by, locator_value))

            eles = self.driver.find_elements(by, locator_value)
            for ele in eles: 
                if ele_displayed(ele):
                    logging.info("[{}] Get child element {}".format(action,locator_value)) 
            return eles            
        except Exception as e:
            logging.error(e)

    def get_ele(self, by , locator_value, action):
        try:
            WebDriverWait(self.driver, 20).until(expected_conditions.visibility_of_any_elements_located((by, locator_value)),
                                                message="[{}] Fail to get element {}:{}".format(action,by, locator_value))

            ele = self.driver.find_element(by, locator_value)
            if ele_displayed(ele):
                logging.info("[{}] Get element {}".format(action,locator_value))
                return ele

        except Exception as e:
            logging.error(e)

    def set_ele(self,by,locator_value, key ,action):
        try:
            WebDriverWait(self.driver, 20).until(expected_conditions.visibility_of_any_elements_located((by, locator_value)),
                                                message="[{}] Fail to set element {}:{}".format(action,by, locator_value))
            ele = self.driver.find_element(by, locator_value)
            if ele_displayed(ele):
                ele.clear()
                ele.send_keys(key)
                logging.info("[{}] Get element and set {} to {}".format(action,locator_value,key))
        except Exception as e:
            logging.error(e)         

    def get_localstorage(self,key):
        return self.driver.execute_script("return window.localStorage.getItem('{}')".format(key))

        