from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from Page.base_page import BasePage 

class AuthPage(BasePage):


     def input_password(self,key):
          super().set_ele(By.XPATH, '//*[@id="mat-input-1"]',key,"Input password")

     def input_email(self,key):
          super().set_ele(By.XPATH, '//*[@id="mat-input-0"]',key,"Input email")

     def click_submit(self):
          super().get_ele(By.XPATH, '//*[@type="submit"]',"Click Submit").click()

     def get_account_name(self):
          text = super().get_ele(By.XPATH,'//*[@class="user-name"]/span',"Click Submit").text
          return text

     def go_to_account(self):
          super().get_ele(By.XPATH,'/html/body/arcade-root/ads-navigation/mat-sidenav-container/mat-sidenav-content/div/mat-toolbar/div/div/div[1]/div[2]/div/div[1]/arcade-login-sign-up-menu/button',"Go to Account").click()

     def click_logout(self):
          super().get_ele(By.XPATH,'//button[text()="Logout"]',"Click Logout Button").click()
     
     def Web_Basic_Login(self,user):
          self.input_password(user.password)
          self.input_email(user.email)
          self.click_submit()