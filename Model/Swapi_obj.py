import Service.api_service as api
import url_data as url
import json
import logging
from data import *

class Swapi:
    
    def get_film(id):
        return  api.get_req(url=url.get_domain('swapi')+url.path("films")+id)

    def get_all_films():
        return api.get_req(url=url.get_domain('swapi')+url.path("films"))
    
    def get_vehicles():
        return  api.get_req(url=url.get_domain('swapi')+url.path("vehicles"))