from collections import deque

ques = [
    {},
    #Q1
    {   "text":"您計畫有多少使用者或裝置將會連線至 Synology NAS？",
        "label": "how_many_people",
        "ele": '//*[@data-question-key="how_many_people"]/section/div[1]/h4/div',
        "type":"ques",
        "option":
        [ 
            { "text":"1 - 5",
              "label":"how_many_people_checkbox_people_less",
              "ele": "//label[@for='how_many_people_checkbox_people_less']",
              "option": None,
              "type":"answer"
            },
            {
               "text": "多於 10",
               "label":"how_many_people_checkbox_people_large",
               "ele":"//label[@for='how_many_people_checkbox_people_large']",
               "option": None,
               "type":"answer"
            }
        ] 
    },
    #Q2
    {

        "text":"您將會在 NAS 上部署多少虛擬機器？",
        "label": "how_many_virtual_machines_for_home",
        "ele":"//*[@data-question-key='how_many_virtual_machines_for_home']/section/div[1]/h4/div",
        "type":"ques",
        "option":
        [ 
            { "text":"少於 2",
              "label": "how_many_virtual_machines_for_home_checkbox_vmm_less",
              "ele":"//label[@for='how_many_virtual_machines_for_home_checkbox_vmm_less']",
              "option": None,
              "type":"answer"
            },
            {
               "text": "3 - 7",
               "label": "how_many_virtual_machines_for_home_checkbox_vmm_medium",
               "ele": "//label[@for='how_many_virtual_machines_for_home_checkbox_vmm_medium']",
               "option": None,
               "type":"answer"
            }
        ] 
    }, 
    #Q3
    {   
        "text":"您的虛擬環境中部署了多少虛擬機器",
        "label": "adjust_vmm_type",
        "ele": '//*[@data-question-key="how_many_people"]/section/div[1]/h4/div',
        "label":"wwww_connet",
        "type":"ques",
        "option":[

            { 
              "text":"一般 Windows 虛擬機器",
              "label": "how_many_virtual_machines_for_home_checkbox_vmm_less",
              "ele":"//*[@id='checkform']/div[1]/section/div[2]/div/div/div/table/tbody/tr[1]/td[4]/input",
              "option": None,
              "type":"answer"
            }          
        ]
    },
    #Q4
    {
        "text":"您將會在 NAS 上部署多少虛擬機器？",
        "label": "how_many_virtual_machines_for_business",
        "ele":"//*[@data-question-key='how_many_virtual_machines_for_business']/section/div[1]/h4/div",
        "type":"ques",
        "option":[

            { 
              "text":"少於 2",
              "label": "how_many_virtual_machines_for_business_checkbox_vmm_less",
              "ele":"//label[@for='how_many_virtual_machines_for_business_checkbox_vmm_less']",
              "option": None,
              "type":"answer"
            },           
            {
                "text":"3 - 7",
                "label":"how_many_virtual_machines_for_business_checkbox_vmm_medium",
                "ele":"//label[@for='how_many_virtual_machines_for_business_checkbox_vmm_medium']",
                "option": None,
                "type":"answer"
            },
        ]
    },
    #Q5
    {
        "text":"您的 NAS 同時會有多少人使用？",
        "label": "how_many_devices",
        "ele":"//*[@data-question-key='how_many_devices']/section/div[1]/h4/div",
        "type":"ques",
        "option":[

            { 
              "text":"少於 20",
              "label": "how_many_devices_checkbox_devices_less_than_20",
              "ele":"//label[@for='how_many_devices_checkbox_devices_less_than_20']",
              "option": None,
              "type":"answer"
            },           
            {
                "text":"20 - 100",
                "label":"how_many_devices_checkbox_devices_less",
                "ele":"//label[@for='how_many_devices_checkbox_devices_less']",
                "option": None,
                "type":"answer"
            },
        ]
    },
    #Q6
    {
        "text":"您有多少同時連線數？",
        "label": "how_many_connection",
        "ele":"//*[@data-question-key='how_many_connection']/section/div[1]/h4/div",
        "type":"ques",
        "option":[

            { 
              "text":"少於 100",
              "label": "how_many_connection_checkbox_connection_less_than_100",
              "ele":"//label[@for='how_many_connection_checkbox_connection_less_than_100']",
              "option": None,
              "type":"answer"
            },           
            {
                "text":"101 - 200",
                "label":"how_many_connection_checkbox_connection_less",
                "ele":"//label[@for='how_many_connection_checkbox_connection_less']",
                "option": None,
                "type":"answer"
            },
        ]
    },
    #Q7
    {
        "text":"您需要影像辨識技術為您自動分類相片嗎？",
        "label": "need_image_recognition",
        "ele":"//*[@data-question-key='how_many_connection']/section/div[1]/h4/div",
        "type":"ques",
        "option":[

            { 
              "text":"需要",
              "label": "need_image_recognition_checkbox_yes_image_reco",
              "ele":"//label[@for='how_many_connection_checkbox_connection_less_than_100']",
              "option": None,
              "type":"answer"
            },           
            {
                "text":"不，我要自己手動分類照片",
                "label":"need_image_recognition_checkbox_no_image_reco",
                "ele":"//label[@for='how_many_connection_checkbox_connection_less']",
                "option": None,
                "type":"answer"
            },
        ]

    }

]


flow_data ={

        "text": "Root",
        "ele": "Root",
        "label": "Root",
        "type": "Root",
        "option":
        [
            {
                "text":"家用或小型辦公室",
                "label": "user_type_home",
                "ele":"//label[@for='user_type_home']",
                "type":"main_option",
                "option": 
                [
                        {   
                            "text": "檔案伺服器與同步",
                            "label": "app_fileserver",
                            "ele":"//label[@for='app_fileserver']",
                            "type":"type_option",
                            "option":[ques[1]]
                        },
                        {
                            "text": "NAS 虛擬機",
                            "label": "app_vmm",
                            "ele": "//label[@for='app_vmm']",
                            "type":"type_option",
                            "option": [ques[2]]
                        },
                        {
                            "text": "多媒體中心",
                            "label": "app_multimedia",
                            "ele": "//label[@for='app_vmm']",
                            "type":"type_option",
                            "option": [ques[7]]

                        },
                        {   
                            "text": "生產力工具",
                            "label": "app_productivity",
                            "ele":"//label[@for='app_productivity']",
                            "type":"type_option",
                            "option":[ques[1]]
                        },
                ]
            },
            {
                "text":"商用或企業用",
                "label": "user_type_business",
                "type":"main_option",
                "option": 
                [
                        {
                            "text": "NAS 虛擬機",
                            "label": "app_vmm",
                            "type":"type_option",
                            "option": [ques[4]]
                        },
                        {
                            "text": "資料備份",
                            "label": "app_databackup",
                            "type":"type_option",
                            "option": [ques[5],ques[6]]
                        }
                ]
            }
        ]
}

class NasOptionTree(object):

    level_flat_tree = []
    sub_data = None
    Parent = {'Root':'Root'}
    level_maping = {"Root":0,"main_option":1,"type_option":2,"ques":3,"answer":4}

    def __init__(self):
        self.flat_tree = self.GetFlatenTree()

    def GetFlatenTree(self,data=flow_data):
        flat = []
        self.DFS(data,flat)
        return flat

    def DFS(self,cur_node,flat):

        if cur_node['option'] is None:
            return

        for node in cur_node['option']:
          self.Parent[node['label']] = (cur_node['text'],cur_node['label'],cur_node['type'])
          flat.append((node['text'],node['label'],node['type']))
          self.DFS(node,flat)
    
    def SetSubData(self,label,limit=2,node=flow_data):

        if node['label'] == label:
            length = len(node['option'])
            node['option'] = node['option'][:min(limit,length)]
            self.sub_data =  node
            return
        
        if node['option'] is None:
            return

        for node in node['option']:
            self.SetSubData(label,limit,node)

    def SetParent(self, data=[]):

        label = data[0][1]
        while label != 'Root':
           data.insert(0,self.Parent[label])
           label = self.Parent[label][1] 
    
        return data       

    '''
    def LevelOrdering(self,node=flow_data):
        que = deque([node])
        while que:
            node = que.popleft()
            self.level_flat_tree.append((node['text'],node['label'],node['type']))
            if node['option'] is not None:
                reverse_op = node['option']
                reverse_op.reverse()
                for node in reverse_op:
                    que.append(node)
    '''


        
            
            

        
            





#tree = NasOptionTree()
