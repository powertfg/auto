import Suite
import logging,sys,unittest,re,argparse,importlib,pkgutil,json,uuid
from datetime import datetime  
from vertify import Vert
from url_data import GlobSet

logging.basicConfig(level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s',
                    handlers=[logging.FileHandler(filename='./logs/'+"{:%Y-%m-%d-%H-%M-%S}".format(datetime.now()) + '.log'),
                              logging.StreamHandler(sys.stderr)]
                    )

parser = argparse.ArgumentParser(description='Select Suits')
parser.add_argument('env',choices=['live','qa'])
parser.add_argument('-s','--suite',nargs='*')
parser.add_argument('-head','--headless',choices=['true','false'])
args = parser.parse_args()

class TextTestResultWithSuccesses(unittest.TextTestResult, Vert):


    def __init__(self, *args, **kwargs):
        super(TextTestResultWithSuccesses, self).__init__(*args, **kwargs)
        self.successes = []

    #Add Success case information, default functions does nothing
    def addSuccess(self, test):
        super(TextTestResultWithSuccesses, self).addSuccess(test)
        test = str(test).split(" ")[0]
        self.successes.append("testMethod=" + test+">")
    #Customize failure inforamtions
    def addFailure(self,test,err):
        super(TextTestResultWithSuccesses, self).addFailure(test,err)
        logging.debug("INSIDE ADDFAILURE"+ str(self.case_assert))
        failmsg = self.failures.pop()
        t = test, str(self.case_assert)
        self.failures.append(t)



def get_suite():
    suite = unittest.TestSuite()
    if(args.suite[0]=='all'):
        s = unittest.TestLoader().discover(pattern="*Suit.py",start_dir='./Suite')
        suite.addTests(s)
        logging.debug("LOAD TESTS ALL:"+str(s)+'\n')
    else:
        module_dict={}
        for finder, name, _ in pkgutil.iter_modules(['Suite']):
            try:
                mod = importlib.import_module('{}.{}'.format(finder.path, name))
                module_dict[mod.__name__] = mod
                logging.debug(module_dict)
            except ImportError as e:
                logger.debug(e)

        for m in args.suite:
            suite.addTests(unittest.TestLoader().loadTestsFromModule(module_dict['Suite.'+m]))
            logging.debug("LOAD TESTS:"+str(suite)+'\n')
    return suite


if __name__ == '__main__':
    logging.info("Start Testing")

    g = GlobSet(args.env)
    if(args.headless == "true"):
        g.enable_headless()

    suites=[]
    if(args.suite[0] == "all"):
        for modname in pkgutil.iter_modules(Suite.__path__):
            suites.append(str(modname.name))
    else:
        suites = args.suite

    testRunner = unittest.runner.TextTestRunner(resultclass=TextTestResultWithSuccesses)
    testResult = testRunner.run(get_suite())

    logging.info("Error: {}" .format(testResult.errors))
    sys.exit()
 