FROM python:3

WORKDIR /app
RUN groupadd --gid 1000 node \
    && useradd --uid 1000 --gid node --shell /bin/bash --create-home node

RUN apt-get update && apt-get install -y git
RUN apt-get install -y git-secret unzip xvfb
RUN apt-get install -y libxss1 libappindicator1 libindicator7 xdg-utils
#RUN apt-get install libgconf2-4 libnss3-1d libxss1

RUN wget -c https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i --force-depends google-chrome-stable_current_amd64.deb
RUN apt-get install -f -y

RUN curl https://chromedriver.storage.googleapis.com/86.0.4240.22/chromedriver_linux64.zip --output driver
RUN unzip driver
RUN chmod +x chromedriver
RUN cp chromedriver /usr/bin/
RUN cp chromedriver /usr/local/bin/

RUN git clone https://gitlab.com/powertfg/auto.git
RUN mkdir logs
RUN cp -a auto/. /app/
RUN chmod +x main.py
RUN pip install requests
RUN pip install selenium
RUN pip install pychrome
RUN echo $PWD
RUN echo $ls
CMD ["python","main.py","live","-s","all","-head","true"]